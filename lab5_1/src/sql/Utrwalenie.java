package sql;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class Utrwalenie {
	public void persist(Object objekt) {
		EntityManagerFactory entitymenagerfactory = javax.persistence.Persistence
				.createEntityManagerFactory("JPAPU");
		EntityManager entitymenager = entitymenagerfactory
				.createEntityManager();
		entitymenager.getTransaction().begin();
		try {
			entitymenager.persist(objekt);
			entitymenager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			entitymenager.getTransaction().rollback();
		} finally {
			entitymenager.close();
		}
	}
	
	public static int main(String [] args){
		Osoba p = new Osoba();
		p.setImie("Grzegorz");
		p.setAdres("Ulica: Test");
		p.setTelefon("500-000-000");
		Utrwalenie u = new Utrwalenie();
		u.persist(p);

		return 0;
		
	}
}
