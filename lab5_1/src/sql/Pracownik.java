package sql;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the pracownik database table.
 * 
 */
@Entity
@Table(name="pracownik")
@NamedQuery(name="Pracownik.findAll", query="SELECT p FROM Pracownik p")
public class Pracownik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String pesel;

	@Column(name="w_brutto")
	private int wBrutto;

	public Pracownik() {
	}

	public String getPesel() {
		return this.pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public int getWBrutto() {
		return this.wBrutto;
	}

	public void setWBrutto(int wBrutto) {
		this.wBrutto = wBrutto;
	}

}