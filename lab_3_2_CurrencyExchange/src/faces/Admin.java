package faces;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import util.NBPConnector;

@ManagedBean
public class Admin {

	private String currency=null;
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency.toUpperCase();
	}

	public String add(){
		String exchange = NBPConnector.exchangeRate(currency);
		if(exchange!=null){
			CurrencyCalculator cc = new CurrencyCalculator();
			cc.getCurrencies().add(currency);
			cc.getExchangeRate().put(currency, Double.parseDouble(exchange.replace(",", ".")));
			return "index.html";
		}else{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.addMessage("addCurrency:currency", new FacesMessage("Bad currency!"));
			return null;
		}
	}
}
