package faces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;

import util.NBPConnector;

@ManagedBean
public class CurrencyCalculator {

	private Double amount = null;
	private String fromCurrency = null;
	private String toCurrency = null;
	private Double result = null;
	private static Map<String, Double> exchangeRate = null;

	private static List<String> currencies = null;

	public CurrencyCalculator() {
		if (currencies == null) {
			currencies = new ArrayList<String>();
			currencies.add("PLN");
			currencies.add("EUR");
			currencies.add("USD");
		}
		if (exchangeRate == null) {
			exchangeRate = new HashMap<String, Double>();
			exchangeRate.put("PLN", 1.0);
			exchangeRate.put("EUR", 4.07032914);
			exchangeRate.put("USD", 3.73594351);
		}
		System.out.println(NBPConnector.exchangeRate("EUR24"));

	}

	public String compute() {
		result = (exchangeRate.get(fromCurrency) * amount)
				/ exchangeRate.get(toCurrency);
		return "index.xhtml";
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getAmount() {
		return amount;
	}

	public String getFromCurrency() {
		return fromCurrency;
	}

	public void setFromCurrency(String fromCurrency) {
		this.fromCurrency = fromCurrency;
	}

	public String getToCurrency() {
		return toCurrency;
	}

	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}

	public List<String> getCurrencies() {
		return currencies;
	}

	public void setCurrencies(List<String> currencies) {
		CurrencyCalculator.currencies = currencies;
	}

	public Double getResult() {
		return result;
	}

	public void setResult(Double result) {
		this.result = result;
	}

	public Map<String, Double> getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(Map<String, Double> exchangeRate) {
		CurrencyCalculator.exchangeRate = exchangeRate;
	}

}
