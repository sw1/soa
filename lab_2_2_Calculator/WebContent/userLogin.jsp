<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="auth" scope="application" class = "calculator.UserLoginServlet" /> 
<c:choose>
	<c:when test='${user!= null}'>
		<jsp:forward page="index.jsp" />
	</c:when>
	<c:otherwise>
		<jsp:setProperty property="pageViewCount" name="auth" value="${auth.pageViewCount+1}"/>
	</c:otherwise>
</c:choose>

<form action="UserLoginServlet" method="post">
Login: ${user} ${AuthError}<br />
Login: <input type="text" name="login"/><br />
Password: <input type="password" name="password" /><br />
Remember me:<input type="checkbox" name="remember" /><br />
<input type="submit" />
</form>
Ilość wejść:${auth.pageViewCount}
</body>
</html>