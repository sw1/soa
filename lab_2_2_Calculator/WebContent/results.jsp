<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="calc" scope="application" class = "calculator.Calculator" /> 
liczba 1:${num1} <br />
liczba 2:${num2} <br />
operacja:${oper} <br />
wynik:${result} <br />
historia:
<%-- ${calc.history} --%>
<c:forEach items="${history}" var="item">
${item}<br />
</c:forEach>
</body>
</html>