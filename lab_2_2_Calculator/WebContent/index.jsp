<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
${user}
<c:choose>
	<c:when test="${user == null}">
		<jsp:forward page="userLogin.jsp" />
	</c:when>
</c:choose>
<form action="CalculatorServlet" method="post">
Liczba 1 <input type="text" name="num1"/><br />
Liczba 2 <input type="text" name="num2"/><br />
<select name="oper">
	<option>dodawanie</option>
	<option>odejmowanie</option>
	<option>mnozenie</option>
	<option>dzielenie</option>
	<option>pierwiastek</option>
</select><br />
<input type="submit" />
</form>
<jsp:useBean id="auth" scope="application" class = "calculator.UserLoginServlet" /> 
Ilość wejść:${auth.pageViewCount}
</body>
</html>