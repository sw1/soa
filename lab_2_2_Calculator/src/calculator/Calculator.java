package calculator;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

	public static List<String> history=new ArrayList<String>();
	
	public List<String> getHistory(){
		return history;
	}
	public double getResult(double num1, double num2, String oper){
		Double result=0.0;
		if(oper.equals("dodawanie")){
			result=num1+num2;
		}else if(oper.equals("odejmowanie")){
			result=num1-num2;
		}else if(oper.equals("mnozenie")){
			result=num1*num2;
		}else if(oper.equals("dzielenie")){
			result=num1/num2;
		}else if(oper.equals("pierwiastek")){
			result=Math.sqrt(num1);
		}
		history.add(String.valueOf(num1)+getSymbol(oper)+num2+"="+result);
		return result;
	}
	
	public String getSymbol(String oper){
		String symbol="";
		if(oper.equals("dodawanie")){
			symbol="+";
		}else if(oper.equals("odejmowanie")){
			symbol="-";
		}else if(oper.equals("mnozenie")){
			symbol="*";
		}else if(oper.equals("dzielenie")){
			symbol="/";
		}else if(oper.equals("pierwiastek")){
			symbol="sqrt";
		}
		return symbol;
	}
}
