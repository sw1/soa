package calculator;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet("/UserLoginServlet")
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	static Integer pageViewCount=0;
	
	static Map<String,String> users = new HashMap<String,String>();
	
	public int getPageViewCount(){
		return pageViewCount;
	}
	
	public void setPageViewCount(int pageViewCount_){
		pageViewCount=pageViewCount_;
	}
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserLoginServlet() {
        super();
        users.put("user", "123");
        users.put("adam", "456");
        users.put("jacek", "789");
        users.put("piotr", "qwe");
        users.put("ala", "asd");
        users.put("amanda", "zxc");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login=request.getParameter("login");
		String pass=request.getParameter("password");
		if(users.containsKey(login) && users.get(login).equals(pass)){
			HttpSession session = request.getSession(true); 
			session.setAttribute("user",login); 
			RequestDispatcher view=request.getRequestDispatcher("index.jsp");
			view.forward(request, response);
		}
		else{
			request.setAttribute("AuthError", "BadPassword");
			RequestDispatcher view=request.getRequestDispatcher("userLogin.jsp");
			view.forward(request, response);
		}
		
	}

}
