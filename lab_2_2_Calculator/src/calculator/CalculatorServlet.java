package calculator;

import java.io.IOException;
import java.nio.channels.SeekableByteChannel;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CalculatorServlet
 */
@WebServlet("/CalculatorServlet")
public class CalculatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CalculatorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true); 
		if(session.getAttribute("user")==null){
			RequestDispatcher view = request.getRequestDispatcher("userLogin.jsp");
			view.forward(request, response);
		}
		double num1=Double.parseDouble(request.getParameter("num1"));
		double num2=Double.parseDouble(request.getParameter("num2"));
		String oper=request.getParameter("oper");
		Calculator calc=new Calculator();
		request.setAttribute("num1", num1);
		request.setAttribute("num2", num2);
		request.setAttribute("oper", oper);
		double result=calc.getResult(num1, num2, oper);
		request.setAttribute("result", result);
		List<String> history=(List<String>) session.getAttribute("history");
		if(history==null) history=new ArrayList<String>();
		history.add(String.valueOf(num1)+calc.getSymbol(oper)+num2+"="+result);
		session.setAttribute("history", history);
		RequestDispatcher view = request.getRequestDispatcher("results.jsp");
		view.forward(request, response);
	}

}
