package calc;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Servlet implementation class Converter
 */
@WebServlet("/Converter")
public class Converter extends HttpServlet {
	
	@EJB
	RemoteConverter conv;
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Converter() {
        super();
       /* try {
        	Context ic = new InitialContext();
			conv = (DegreeConverter) ic.lookup(DegreeConverter.class.getName());
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
    }

    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		double temp = Double.parseDouble(request.getParameter("temp"));
		String type = request.getParameter("type");
		double result = 0.0;
		String from=null,to=null;
		if (type.equals("c2f")){
			result=conv.Cels2Fahr(temp);
			from="C";to="F";
		}else if(type.equals("f2c")) {
			result=conv.Fahr2Cels(temp);
			from="F";to="C";
		}
		response.setContentType("text/plain");
		PrintWriter pw=response.getWriter();
		pw.println(String.valueOf(temp)+from+" is "+result+to);
	}

}
