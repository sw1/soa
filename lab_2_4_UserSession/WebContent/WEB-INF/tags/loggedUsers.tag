<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"
     prefix="c" %>
     
<%@ attribute name="order" required="true" %>
<%@ attribute name="color" %>
<jsp:useBean id="loggedUsers" scope="application" class = "session.LoggedUser" /> 
<c:forEach items="${order.equals('desc')?loggedUsers.loggedUsersDesc:loggedUsers.loggedUsersAsc}" var="user">
	${user.login} <span style="color:{$color}">${user.date}</span><br />
</c:forEach>