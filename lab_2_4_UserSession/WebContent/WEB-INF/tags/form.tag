<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"
     prefix="c" %>
<%@ attribute name="user" required="true" rtexprvalue="true" %>
<c:choose>
	<c:when test="${user != null }">
		<a href="UserSession?logout=1">Logout</a><br />
	</c:when>
	<c:otherwise>
		<form action="UserSession" method="post">
			Login: <input type="text" name="login" />
			Password: <input type="password" name="password" /> <br />
			<input type="submit" />
		</form>
	</c:otherwise>
</c:choose>