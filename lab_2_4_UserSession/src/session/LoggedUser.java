package session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


public class LoggedUser {
	public class UserLoginDetail{
		public String login=null;
		public String date=null;;
		public UserLoginDetail(String login, String date) {
			this.login=login;
			this.date=date;
		}
		
		public boolean equals(Object o){
			if(o==null)
				return false;
			return login.equals(o);
		}
		
		public int hashcode(){
			return login.hashCode();
		}
		
		public String getDate(){
			return date;
		}
		
		public String getLogin(){
			return login;
		}
	}
	static List<UserLoginDetail> loggedUsers=new ArrayList<UserLoginDetail>();
	public List<UserLoginDetail> getLoggedUsers(){
		return loggedUsers;
	}
	public void addLoggedUser(String user, String date){
		loggedUsers.add(new UserLoginDetail(user, date));
	}
	public void removeLoggedUser(String user){
		UserLoginDetail usd=null;
		Iterator<UserLoginDetail> i=loggedUsers.listIterator();
		while(i.hasNext()){
			usd=i.next();
			if(usd.equals(user))
				i.remove();
		}
	}
	
	public List<UserLoginDetail> getLoggedUsersAsc(){
		Collections.sort(loggedUsers, new Comparator<UserLoginDetail>() {
			public int compare(UserLoginDetail o1, UserLoginDetail o2) {
				return o1.date.compareTo(o2.date);
			}
		});
		return loggedUsers;
	}
	
	public List<UserLoginDetail> getLoggedUsersDesc(){
		Collections.sort(loggedUsers, new Comparator<UserLoginDetail>() {
			public int compare(UserLoginDetail o1, UserLoginDetail o2) {
				return -o1.date.compareTo(o2.date);
			}
		});
		return loggedUsers;
	}
	
}
