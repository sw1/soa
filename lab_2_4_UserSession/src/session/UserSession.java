package session;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class UserSession
 */
@WebServlet("/UserSession")
public class UserSession extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Map<String,String> users = new HashMap<String,String>();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSession() {
        super();
        users.put("user", "123");
        users.put("adam", "456");
        users.put("jacek", "789");
        users.put("piotr", "qwe");
        users.put("ala", "asd");
        users.put("amanda", "zxc");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("logout")!=null) {
			HttpSession session = request.getSession(true); 
			LoggedUser lu=new LoggedUser();
			lu.removeLoggedUser((String) session.getAttribute("user"));
			session.setAttribute("user",null); 
		}
		RequestDispatcher view=request.getRequestDispatcher("index.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login=request.getParameter("login");
		String pass=request.getParameter("password");
		if(users.containsKey(login) && users.get(login).equals(pass)){
			LoggedUser lu=new LoggedUser();
			HttpSession session = request.getSession(true);
			lu.addLoggedUser(login, String.valueOf(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX")
	        .format(new Date())));
			session.setAttribute("user",login); 
			RequestDispatcher view=request.getRequestDispatcher("index.jsp");
			view.forward(request, response);
		}
		else{
			request.setAttribute("AuthError", "BadPassword");
			RequestDispatcher view=request.getRequestDispatcher("index.jsp");
			view.forward(request, response);
		}
	}

}
