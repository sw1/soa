package formating;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class FormatParagraphTag extends  SimpleTagSupport{
	private String body=null;
	private String align=null;
	private String color=null;
	public void setBody(String body){
		this.body=body;
	}
	
	public void setAlign(String align){
		this.align=align;
	}
	
	public void setColor(String color){
		this.color=color;
	}
	
	public void doTag() throws JspException, IOException {
        JspWriter w=getJspContext().getOut();
        w.print("<p style=\"");
        if (align!=null && (align.equals("left") || align.equals("right"))){
        	w.print("text-align: "+align+";");
        }
        if(color!=null){
        	w.print("color:"+color+";");
        }
        w.print("\" >"+body+"</p>");
        w.println();
	}

}
