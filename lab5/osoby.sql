-- phpMyAdmin SQL Dump
-- version 2.6.0-pl2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Czas wygenerowania: 19 Maj 2008, 13:39
-- Wersja serwera: 3.23.58
-- Wersja PHP: 4.3.9
-- 
-- Baza danych: `osoby`
-- 

-- --------------------------------------------------------

-- 
-- Struktura tabeli dla  `miasto`
-- 

CREATE TABLE `miasto` (
  `ID` bigint(20) NOT NULL auto_increment,
  `NAZWA` varchar(255) default NULL,
  PRIMARY KEY  (`ID`)
) TYPE=MyISAM AUTO_INCREMENT=4 ;

-- 
-- Zrzut danych tabeli `miasto`
-- 

INSERT INTO `miasto` VALUES (1, 'lodz');
INSERT INTO `miasto` VALUES (2, 'warszawa');
INSERT INTO `miasto` VALUES (3, 'zamosc');

-- --------------------------------------------------------

-- 
-- Struktura tabeli dla  `studenci`
-- 

CREATE TABLE `studenci` (
  `ID` bigint(20) NOT NULL auto_increment,
  `IMIE` varchar(255) default NULL,
  `NAZWISKO` varchar(255) default NULL,
  `OCENA` float default NULL,
  `JEZYKI` varchar(255) default NULL,
  `ID_MIASTO` bigint(20) default NULL,
  PRIMARY KEY  (`ID`)
) TYPE=MyISAM AUTO_INCREMENT=6 ;

-- 
-- Zrzut danych tabeli `studenci`
-- 

INSERT INTO `studenci` VALUES (3, 'Jan', 'Nowak', 0, NULL, 2);
INSERT INTO `studenci` VALUES (5, 'Anna', 'Kowalczyk', 0, NULL, 3);
