package database;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@NamedQueries({ @NamedQuery(name = "getCategoryById", query = "FROM categories c WHERE c.categoryid = :catid") ,
	@NamedQuery(name = "getAllCategories", query = "FROM categories")})
@Entity(name="categories")
@Table(name = "categories")

public class Category implements Serializable{

	private static final long serialVersionUID = 3385430071502110001L;

	
	public enum CategoryType {
		TOWER, FOREST, CAVE
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "categoryid")
	private int categoryid;

	@ManyToOne
	@JoinColumn(name = "userid")
	@JsonBackReference
	private User user;
	
	@Column
	@Enumerated
	private CategoryType type;

	@Column
	private int property;

	@OneToMany(mappedBy = "category",fetch=FetchType.EAGER,orphanRemoval=true)
	@Cascade({ CascadeType.ALL})
	@JsonManagedReference
	private Set<Element> elements;

	public int getCategoryid() {
		return categoryid;
	}

	public void setCategoryid(int columnid) {
		this.categoryid = columnid;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public CategoryType getType() {
		return type;
	}

	public void setType(CategoryType type) {
		this.type = type;
	}

	public int getProperty() {
		return property;
	}

	public void setProperty(int property) {
		this.property = property;
	}

	public Set<Element> getElements() {
		return elements;
	}

	public void setElements(Set<Element> elements) {
		this.elements = elements;
	}

	@JsonIgnore
	public CategoryType[] getTypes() {
		return CategoryType.values();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + categoryid;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (categoryid != other.categoryid)
			return false;
		return true;
	}

}
