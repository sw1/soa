package database;

import java.io.Serializable;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;


@NamedQueries({
		@NamedQuery(name = "getBestElements", query = "FROM elements e WHERE e.type = :etype ORDER BY e.resources DESC LIMIT 3"),
		@NamedQuery(name = "getBestElementsResource", query = "FROM elements e WHERE e.type = :etype ORDER BY e.resources DESC LIMIT 1") })
@Entity(name = "elements")
@Table(name = "elements")
public class Element implements Serializable {

	private static final long serialVersionUID = -4145084725384964181L;

	public enum ElementType {
		DRAGON, WIZARD, ELF
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "elementid")
	private int elementid;
	@ManyToOne
	@JoinColumn(name = "categoryid")
	@JsonBackReference
	private Category category;
	@Column
	private String name;
	@Column
	private int resources;
	@Column
	private String attribute;
	@Enumerated
	private ElementType type;

	public int getElementid() {
		return elementid;
	}

	public void setElementid(int elementid) {
		this.elementid = elementid;
	}
	
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getResources() {
		return resources;
	}

	public void setResources(int resources) {
		this.resources = resources;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public ElementType getType() {
		return type;
	}

	public void setType(ElementType type) {
		this.type = type;
	}

	@JsonIgnore
	public ElementType[] getTypes() {
		return ElementType.values();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + elementid;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Element other = (Element) obj;
		if (elementid != other.elementid)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Element [elementid=" + elementid + ", "
				+ (category != null ? "category=" + category + ", " : "")
				+ (name != null ? "name=" + name + ", " : "") + "resources="
				+ resources + ", "
				+ (attribute != null ? "attribute=" + attribute + ", " : "")
				+ (type != null ? "type=" + type : "") + "]";
	}

}
