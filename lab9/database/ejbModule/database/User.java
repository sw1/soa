package database;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.picketbox.commons.cipher.Base64;


@NamedQueries({
		@NamedQuery(name = "getUserByUsername", query = "FROM users u WHERE u.login = :username"),
		@NamedQuery(name = "findUsers", query = "FROM users") })
@Entity(name = "users")
@Table(name = "users")
public class User implements Serializable {

	private static final long serialVersionUID = 7563524582563959328L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "userid")
	private int userid;

	@Column
	private String login;

	@Column
	private String password;

	@OneToMany(mappedBy = "user",fetch=FetchType.EAGER,orphanRemoval=true)
	@Cascade({ CascadeType.ALL })
	@JsonManagedReference
	private Set<Category> categories;

	@OneToMany(mappedBy = "userid", fetch=FetchType.EAGER)
	@Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
	@JsonManagedReference
	private List<Role> roles;

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(password.getBytes());
			this.password = Base64.encodeBytes(md.digest());
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		return true;
	}
	
	public boolean hasRole(String role){
		boolean inRole = false;
		for(Role r:getRoles()){
			if(r.getRole().equalsIgnoreCase(role)){
				inRole=true;
				break;
			}
		}
		return inRole;
	}

}
