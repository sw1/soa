package database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

/**
 * Entity implementation class for Entity: roles
 *
 */
@Entity(name="roles")
@Table(name="roles")
public class Role implements Serializable {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="roleid")
	private int roleid;
	
	@ManyToOne
	@JoinColumn(name="userid")
	@JsonBackReference
	private User userid;
	
	@Column
	private String role;
	private static final long serialVersionUID = 1L;

	public Role() {
		super();
	}   
	public User getUserid() {
		return this.userid;
	}

	public void setUserid(User userid) {
		this.userid = userid;
	}   
	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	public int getRoleid() {
		return roleid;
	}
	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}
   
	
}
