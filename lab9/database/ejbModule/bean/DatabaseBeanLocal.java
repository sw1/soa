package bean;

import java.util.List;
import java.util.Set;

import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Local;
import javax.ejb.Remote;

import database.*;
import database.Category.CategoryType;
import database.Element.ElementType;

@Local
public interface DatabaseBeanLocal {
	User getUserByUsername(String username);
	List<User> returnUsers();
	List<Category> returnCategories();
	List<Element> returnElements();
	void saveOrUpdateUser(User user);
	void updateUser(User user);
	void saveOrUpdateCategory(Category category);
	void mergeCategory(Category category);
	void saveOrUpdateElement(Element element);
	void deleteElement(Element element);
	void deleteCategory(Category category);
	void deleteUser(User user);
	Category getCategoryById(int id);
	public User getLoggedUser();
	public List<Element> getBestElements(ElementType type);
	public Integer getMaxElementResource(ElementType type);
	public List<Category> getRestCategories();
}
