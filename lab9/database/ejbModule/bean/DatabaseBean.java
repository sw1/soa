package bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;

import database.Category;
import database.Category.CategoryType;
import database.Element.ElementType;
import database.Element;
import database.User;

/**
 * Session Bean implementation class DatabaseBean
 */

@Stateful
public class DatabaseBean implements DatabaseBeanLocal {

	@PersistenceContext(unitName = "persistence")
	private EntityManager entityManager;

	@Resource
	private SessionContext context;

	/**
	 * Default constructor.
	 */
	public DatabaseBean() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<User> returnUsers() {
		Session session = (Session) entityManager.getDelegate();
		Transaction transaction = null;
		List<User> users = null;
		try {
			transaction = session.beginTransaction();
			User user = getLoggedUserInSession();
			if (user.hasRole("admin")) {
				Query query = session.getNamedQuery("findUsers");
				users = query.list();
			} else {
				users = new ArrayList<>();
				users.add(user);
			}
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}
		return users;
	}

	public User getLoggedUser() {
		User u = null;
		Session session = (Session) entityManager.getDelegate();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			u = getUserByUsername(context.getCallerPrincipal().getName());
			;
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}
		return u;
	}

	private User getLoggedUserInSession() {

		return getUserByUsername(context.getCallerPrincipal().getName());
	}

	@Override
	public List<Category> returnCategories() {
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		List<Category> categories = null;
		try {
			transaction = session.beginTransaction();
			if (getLoggedUserInSession().hasRole("admin")) {
				Query query = session.getNamedQuery("getAllCategories");
				categories = query.list();
			} else {
				categories = new ArrayList<>(getLoggedUserInSession()
						.getCategories());
			}
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}
		return categories;
	}

	public List<Category> getRestCategories() {
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		List<Category> categories = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.getNamedQuery("getAllCategories");
			categories = query.list();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}
		return categories;
	}

	@Override
	public List<Element> returnElements() {
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		List<Element> elements = null;
		try {
			transaction = session.beginTransaction();
			Criteria cr = session.createCriteria(Element.class);
			elements = cr.list();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}
		return elements;
	}

	@Override
	public void saveOrUpdateUser(User user) {
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(user);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			// session.close();
		}
	}

	@Override
	public void saveOrUpdateCategory(Category category) {
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			if (category.getUser() == null) {
				User s = getLoggedUserInSession();
				s.getCategories().add(category);
				category.setUser(s);
				session.save(category);
			} else {
				session.merge(category);
			}

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}

	}

	@Override
	public void mergeCategory(Category category) {
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.merge(category);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}

	}

	@Override
	public void saveOrUpdateElement(Element element) {
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Category c = element.getCategory();
			if (!c.getElements().contains(element)) {
				c.getElements().add(element);
				session.saveOrUpdate(element);
			} else {
				session.merge(element);
			}

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}
	}

	@Override
	public void deleteElement(Element element) {
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Element e = (Element) (session.contains(element) ? element
					: session.merge(element));
			Category c = e.getCategory();
			c.getElements().remove(e);
			e.setCategory(null);
			session.saveOrUpdate(c);
			session.flush();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}

	}

	@Override
	public void deleteCategory(Category category) {
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Category c = (Category) (session.contains(category) ? category
					: session.merge(category));
			User u = category.getUser();
			u.getCategories().remove(category);
			c.setUser(null);
			session.merge(u);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}
	}

	@Override
	public void deleteUser(User user) {
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.delete(user);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}

	}

	@Override
	public Category getCategoryById(int id) {
		Category category = null;
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.getNamedQuery("getCategoryById");
			query.setInteger("catid", id);
			category = (Category) query.uniqueResult();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}
		return category;
	}

	@Override
	public User getUserByUsername(String username) {
		User user = null;
		Session session = entityManager.unwrap(Session.class);
		Query query = session.getNamedQuery("getUserByUsername");
		query.setString("username", username);
		user = (User) query.uniqueResult();
		return user;
	}

	@Override
	public void updateUser(User user) {
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.merge(user);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}
	}

	@Override
	public List<Element> getBestElements(ElementType type) {
		List<Element> bestElements = null;
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.getNamedQuery("getBestElements");
			query.setParameter("etype", type);
			query.setMaxResults(3);
			bestElements = query.list();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}
		return bestElements;
	}

	@Override
	public Integer getMaxElementResource(ElementType type) {
		Integer bestResource = null;
		Session session = entityManager.unwrap(Session.class);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.getNamedQuery("getBestElementsResource");
			query.setParameter("etype", type);
			query.setMaxResults(1);
			Element el = (Element) query.uniqueResult();
			bestResource = el == null ? Integer.MAX_VALUE : el.getResources();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();

		} finally {
			// session.close();
		}
		return bestResource;
	}

}
