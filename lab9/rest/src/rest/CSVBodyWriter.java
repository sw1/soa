package rest;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ejb.Singleton;
import javax.swing.text.Element;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import database.Category;

@Singleton
@Produces("text/csv")
@Provider
public class CSVBodyWriter implements MessageBodyWriter<Category> {

	private String getResult(Category c) {
		String s = null;
		s = c.getCategoryid() + "," + c.getType() + "," + c.getProperty();
		s += ",[";
		for (database.Element e : c.getElements()) {
			s += "<" + e.getName() + "," + e.getResources() + ","
					+ e.getAttribute() + ">";
		}
		s += "]";
		return s;
	}

	@Override
	public long getSize(Category c, Class<?> arg1, Type arg2,
			Annotation[] arg3, MediaType arg4) {
		return getResult(c).length();
	}

	@Override
public void writeTo(Category c, Class<?> type, Type genericType, Annotation[] annotations,
		MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream out)
		throws IOException, WebApplicationException {
	Writer writer = new OutputStreamWriter(out);
	writer.write(getResult(c));
	writer.flush();
}

	@Override
	public boolean isWriteable(Class<?> arg0, Type arg1, Annotation[] arg2,
			MediaType arg3) {
		// TODO Auto-generated method stub
		return true;
	}

}
