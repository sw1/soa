package rest;

import java.net.HttpURLConnection;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import bean.DatabaseBeanLocal;
import database.Category;
import database.Element;


@Path("/categories")
public class RestService {

	@EJB
	DatabaseBeanLocal db;
	
	@GET
	@Path("/")
	@Produces("application/json")
	public List<Category> getCategories(){
		return db.getRestCategories();
	}
	
	@GET
	@Produces("application/json")
	@Path("/{id}")
	public Category getCategory(@PathParam("id") String id){
		Category c = db.getCategoryById(Integer.parseInt(id));
		if(c==null)
			throw new WebApplicationException(HttpURLConnection.HTTP_NOT_FOUND);
		return c;
	}
	
	@GET
	@Produces("text/csv")
	@Path("/{id}")
	public Category getCategoryPostscript(@PathParam("id") String id){
		Category c = db.getCategoryById(Integer.parseInt(id));
		if(c==null)
			throw new WebApplicationException(HttpURLConnection.HTTP_NOT_FOUND);
		return c;
	}
	
	@POST
	@Produces("application/json")
	@Path("/{id}")
	public Response addElement(@PathParam("id") String id, Element e){
		Category c = db.getCategoryById(Integer.parseInt(id));
		if(c==null)
			throw new WebApplicationException(HttpURLConnection.HTTP_NOT_FOUND);
		e.setCategory(c);
		db.saveOrUpdateElement(e);
		return Response.status(200).entity("Success").build();
	}
	
}
