import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RESTApp {

	public void printCategories() throws MalformedURLException, IOException{
		URL url = new URL(
				"http://localhost:8080/rest/categories/");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}
		
		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
		
		String output;
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}
		
		
		conn.disconnect();
	}
	
	public void sendElement() throws IOException{
		URL url = new URL(
				"http://localhost:8080/rest/categories/100");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		String input = "{\"name\":\"re2s14t\",\"resources\":2,\"attribute\":\"light\",\"type\":\"WIZARD\"}";
		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();
		
		if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}
		
		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
		
		String output;
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}
		
		
		conn.disconnect();
	}
	
	public static void main(String[] args) {
		RESTApp r = new RESTApp();
		try{
			r.printCategories();
			r.sendElement();
			r.printCategories();
		}catch(IOException e){
			e.printStackTrace();
		}
	}

}
