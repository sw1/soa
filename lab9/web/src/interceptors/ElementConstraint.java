package interceptors;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import database.Element;
import bean.DatabaseBeanLocal;
import view.EditElement;

@Constrain
@Interceptor
public class ElementConstraint implements Serializable{

	private static final long serialVersionUID = 3826606645189946004L;
	@Inject
	EditElement editElement;
	@EJB
	DatabaseBeanLocal db;
	
	public void ElementConstrain() {

	}

	@AroundInvoke
	public Object constrainElement(InvocationContext invocationContext)
			throws Exception {
		Element el=editElement.getElement();
		Integer max = db.getMaxElementResource(el.getType());
		if (el.getResources()>max && el.getElementid()==0){
			el.setResources(max);
		}
		return invocationContext.proceed();
	}

}
