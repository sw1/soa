package view;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import database.Category;
import bean.DatabaseBeanLocal;
@ManagedBean
public class CategoryConverter implements Converter{
	@EJB
	DatabaseBeanLocal db;
	@Override
	public Category getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		return db.getCategoryById(Integer.valueOf(value));
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		// TODO Auto-generated method stub
		return String.valueOf(((Category) arg2).getCategoryid());
	}

}
