package view;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import database.User;
import bean.DatabaseBeanLocal;

@ManagedBean
public class UserConverter implements Converter {
	@EJB
	DatabaseBeanLocal db;

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		return db.getUserByUsername(arg2);
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		return ((User) arg2).getLogin();
	}

}
