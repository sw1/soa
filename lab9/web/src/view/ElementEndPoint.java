package view;

import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.impl.JSONEncoder;

@PushEndpoint("/elementEvent")
public class ElementEndPoint {
	
	@OnMessage(encoders = {JSONEncoder.class})
    public String onMessage(String element) {
        return "message";
    }
}
