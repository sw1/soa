package view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

import database.*;
import database.Category.CategoryType;
import database.Element.ElementType;
import bean.DatabaseBeanLocal;

@SessionScoped
@Named
public class Display implements Serializable {

	private static final long serialVersionUID = -8424336799759871751L;

	@EJB
	DatabaseBeanLocal db;

	private User user;
	private Category category;
	private Element element;
	private String password = "";
	private CategoryType userCategoryType;
	private ElementType userElementType;

	public List<User> getUsers() {
		return db.returnUsers();
	}

	public User getUser() {
		return user;
	}

	public User getLoggedUser() {
		return db.getLoggedUser();
	}

	public List<Category> getCategories() {
		List<Category> categories = null;
		categories = db.returnCategories();
		return categories;
	}

	public List<Element> getElements() {
		return db.returnElements();
	}

	public String deleteElement() {
		db.deleteElement(element);
		element = null;
		category = null;
		EventBus eventBus = EventBusFactory.getDefault().eventBus();
        eventBus.publish("/elementEvent","element");
		return "view.xhtml";
	}

	public String deleteCategory() {
		db.deleteCategory(category);
		element = null;
		category = null;
		return "view.xhtml";
	}

	public String addOrModifyElement() {
		db.saveOrUpdateElement(element);
		element = null;
		category = null;
		EventBus eventBus = EventBusFactory.getDefault().eventBus();
        eventBus.publish("/elementEvent","element");
		return "view.xhtml";
	}

	public String addOrModifyCategory() {
		db.saveOrUpdateCategory(category);
		category = null;
		element = null;
		return "view.xhtml";
	}

	public void elAttrListener(ActionEvent event) {
		element = (Element) event.getComponent().getAttributes().get("element");
		deleteElement();
	}

	public void catAttrListener(ActionEvent event) {
		category = (Category) event.getComponent().getAttributes()
				.get("category");
	}

	public Category getCategory() {
		if (category == null) {
			category = new Category();
		}
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Element getElement() {
		if (element == null)
			element = new Element();
		return element;
	}

	public void setElement(Element element) {
		this.element = element;
	}

	public String logout() {
		FacesContext.getCurrentInstance().getExternalContext()
				.invalidateSession();
		return "/index.xhtml?faces-redirect=true";
	}

	public List<Category> getCategoryTypes(Element e) {
		List<Category> catTypes = new ArrayList<Category>();
		if(e!=null && e.getCategory()!=null){
			catTypes.add(e.getCategory());
		}else{
			catTypes.addAll(getLoggedUser().getCategories());
		}
		return catTypes;
	}
	
	public List<String> getCategoryTypes() {
		List<String> catTypes = new ArrayList<String>();
		if(category.getType()!=null){
			catTypes.add(category.getType().toString());
		}else{
			Integer userid = getLoggedUser().getUserid();
			if (userid % 3 == 0 && userid % 2 != 0) {
				catTypes.add(CategoryType.CAVE.toString());
			} else if (userid % 2 == 0 && userid % 3 != 0) {
				catTypes.add(CategoryType.TOWER.toString());
			} else {
				catTypes.add(CategoryType.FOREST.toString());
			}
		}
		return catTypes;
	}

	public List<String> getElementTypes() {
		List<String> elTypes = new ArrayList<String>();
		User user = getLoggedUser();
		Category c = element == null ? null : element.getCategory();
		if (c != null) {
			if (c.getType() == CategoryType.CAVE) {
				elTypes.add(ElementType.DRAGON.toString());
			} else if (c.getType() == CategoryType.TOWER) {
				elTypes.add(ElementType.WIZARD.toString());
			} else {
				elTypes.add(ElementType.ELF.toString());
			}
		} else {
			Integer userid = user.getUserid();
			if (userid % 3 == 0 && userid % 2 != 0) {
				elTypes.add(ElementType.DRAGON.toString());
			} else if (userid % 2 == 0 && userid % 3 != 0) {
				elTypes.add(ElementType.WIZARD.toString());
			} else {
				elTypes.add(ElementType.ELF.toString());
			}
		}
		return elTypes;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String updateUserPassword() {
		user.setPassword(password);
		db.updateUser(user);
		return "index.html";
	}

	public String getResourceName(ElementType e) {
		String name = "";
		if (e == ElementType.DRAGON) {
			name = "Gold";
		} else if (e == ElementType.ELF) {
			name = "Arrow Count";
		} else if (e == ElementType.WIZARD) {
			name = "Magic Points";
		}
		return name;
	}

	public String getAttributeName(ElementType e) {
		String name = "";
		if (e == ElementType.DRAGON) {
			name = "Color";
		} else if (e == ElementType.ELF) {
			name = "Arrow type";
		} else if (e == ElementType.WIZARD) {
			name = "Element";
		}
		return name;
	}

	public String getResourceName() {
		return getResourceName(getUserElementType());
	}

	public String getAttributeName() {
		return getAttributeName(getUserElementType());
	}

	public String getCategoryResource() {
		String name = "";
		User user = getLoggedUser();
		Integer userid = user.getUserid();
		if (userid % 3 == 0 && userid % 2 != 0) {
			name = "Area";
		} else if (userid % 2 == 0 && userid % 3 != 0) {
			name = "Height";
		} else {
			name = "Tree count";
		}
		return name;
	}

	public ElementType getElementTypeFromCategory(CategoryType c) {
		ElementType type = null;
		if (c == CategoryType.CAVE) {
			type = ElementType.DRAGON;
		} else if (c == CategoryType.TOWER) {
			type = ElementType.WIZARD;
		} else {
			type = ElementType.ELF;
		}
		return type;
	}

	public String getUserIdResource() {
		return getResourceName(getUserElementType());
	}

	public String getUserIdAttribute() {
		return getAttributeName(getUserElementType());
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public CategoryType getUserCategoryType() {
		if (userCategoryType == null) {
			Integer userid = getLoggedUser().getUserid();
			if (userid % 3 == 0 && userid % 2 != 0) {
				userCategoryType = CategoryType.CAVE;
			} else if (userid % 2 == 0 && userid % 3 != 0) {
				userCategoryType = CategoryType.TOWER;
			} else {
				userCategoryType = CategoryType.FOREST;
			}
		}
		return userCategoryType;
	}

	public ElementType getUserElementType() {
		if (userElementType == null) {
			Integer userid = getLoggedUser().getUserid();
			if (userid % 3 == 0 && userid % 2 != 0) {
				userElementType = ElementType.DRAGON;
			} else if (userid % 2 == 0 && userid % 3 != 0) {
				userElementType = ElementType.WIZARD;
			} else {
				userElementType = ElementType.ELF;
			}
		}
		return userElementType;
	}

}
