package view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

import bean.DatabaseBeanLocal;
import database.Element;
import database.Element.ElementType;

@ApplicationScoped
@Named
public class Stats implements Serializable {
	private static final long serialVersionUID = -4080617342105008367L;

	@Inject
	Display display;
	@EJB
	DatabaseBeanLocal db;
	Map<Element.ElementType, List<Element>> bestElementsMap = new HashMap<Element.ElementType, List<Element>>();
	
	public void editElementHandler(@Observes @Any ElementEvent event) {
		bestElementsMap.remove(event.getElement().getType());
		bestElementsMap.put(event.getElement().getType(),
				db.getBestElements(event.getElement().getType()));
		EventBus eventBus = EventBusFactory.getDefault().eventBus();
        eventBus.publish("/elementEvent","element");
	}

	public List<Element> getBestElements() {
		List<Element> bestElements = null;
		ElementType type = display.getUserElementType();
		if (bestElementsMap.get(type) == null) {
			bestElementsMap.put(type, db.getBestElements(type));
		}
		bestElements = bestElementsMap.get(type);
		return bestElements;
	}
}
