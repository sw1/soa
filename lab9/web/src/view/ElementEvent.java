package view;

import database.Element;

public class ElementEvent {

	private Element element;

	public Element getElement() {
		return element;
	}

	public void setElement(Element element) {
		this.element = element;
	}
	
	@Override
	public String toString() {
		return element.toString();
	}
	
}
