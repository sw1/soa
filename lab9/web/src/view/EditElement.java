package view;

import interceptors.Constrain;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.interceptor.Interceptors;

import bean.DatabaseBeanLocal;
import database.Category;
import database.Element;

@ConversationScoped
@Named
public class EditElement implements Serializable {

	private static final long serialVersionUID = 7148765841395537228L;

	@EJB
	DatabaseBeanLocal db;
	
	@Inject
	private Conversation conversation;
	
	@Inject
	Display display;
	
	@Inject
	@Any
	Event<ElementEvent> elementEvent;
	
	Element element;
	
	public Element getElement() {
		if (element == null){
			setElement(new Element());
		}
		return element;
	}

	public void setElement(Element element) {
		conversation.begin();
		this.element = element;
	}

	public void attrListener(ActionEvent event) {
		setElement((Element) event.getComponent().getAttributes().get("element"));
	}
	
	@Constrain
	public String confirm(){
		return "confirm_element.xhtml";
	}
	
	public List<Category> getCategories(){
		return display.getCategoryTypes(element);
	}
	
	public String save(){
		db.saveOrUpdateElement(element);
		conversation.end();
		ElementEvent elementPayload = new ElementEvent();
		elementPayload.setElement(element);
		elementEvent.fire(elementPayload);
		return "view.xhtml";
	}
	
	public String cancel(){
		conversation.end();
		return "view.xhtml";
	}
	
}
