package filters;

import java.io.IOException;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyFilter implements javax.servlet.Filter {
	private javax.servlet.FilterConfig myFilterConfig;

	public MyFilter() {
	}

	public void doFilter(javax.servlet.ServletRequest servletRequest,
			javax.servlet.ServletResponse servletResponse,
			javax.servlet.FilterChain filterChain) throws java.io.IOException,
			javax.servlet.ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse)servletResponse;
		String useragent = request.getHeader("User-Agent").toLowerCase();
		if (!useragent.contains("firefox")) {
			response.getWriter().write("Use firefox! <a href=\"http://www.mozilla.org/firefox/\">https://www.mozilla.org/firefox/</a>\n");
			response.flushBuffer();
			return;
		}
		filterChain.doFilter(servletRequest, servletResponse);
	}

	public javax.servlet.FilterConfig getFilterConfig() {
		return myFilterConfig;
	}

	public void setFilterConfig(javax.servlet.FilterConfig filterConfig) {
		myFilterConfig = filterConfig;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}
}
