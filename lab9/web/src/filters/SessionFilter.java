package filters;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.*;
import javax.servlet.http.*;

public class SessionFilter implements Filter {
	private Map<String, HttpSession> sessions = new HashMap<String, HttpSession>();

	public void init(FilterConfig config) throws ServletException {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest servletRequest,
			ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		Principal principal = request.getUserPrincipal();
		HttpSession session = request.getSession();

		if (principal != null && session.getAttribute("THE_PRINCIPAL") == null) {

			// update the current session
			session.setAttribute("THE_PRINCIPAL", session);

			// get the username from the principal
			// (assumes you using container based authentication)
			String username = principal.getName();

			// invalidate previous session if it exists
			HttpSession s = sessions.get(username);
			if (s != null) {
				try {
					long sd = s.getCreationTime();
					session.invalidate();
					response.getWriter().write("User already logged in!\n");
					return;
				} catch (IllegalStateException ise) {
					sessions.remove(username);
				}
				
			}

			// register this session as the one for the user
			sessions.put(username, session);

		}

		chain.doFilter(servletRequest, servletResponse);

	}

}