<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:useBean id="db" class="hibernate.DBManager"></jsp:useBean>
	<table>
		<c:forEach items="${db.books}" var="book">
			<tr>
				<td>${book.isbn}</td>
				<td>${book.title}</td>
				<td>${book.aurthor}</td>
				<td>${book.year}</td>
				<td><a
					href="BookActionManagerServlet?action=edit&isbn=${book.isbn}">Edit</a></td>
				<td><a
					href="BookActionManagerServlet?action=remove&isbn=${book.isbn}">Remove</a></td>
			</tr>
		</c:forEach>
	</table>
	<a href="bookEdit.jsp">Add book</a>
</body>
</html>