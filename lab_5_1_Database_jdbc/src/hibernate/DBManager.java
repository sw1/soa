package hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;


public class DBManager {

	private static final SessionFactory sessionFactory;
	static {
		try {
			sessionFactory = new AnnotationConfiguration().configure()
					.buildSessionFactory();
		} catch (Throwable ex) {
			System.out.println("Błąd w inicjalizacji SessionFactory" + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	public void createConnection() {

	}

	public Book getBook(String isbn) {
		Book b = new Book();

		return b;
	}

	public void editBook(Book b) {

	}

	public void addBook(Book b) {

	}

	public void removeBook(String isbn) {

	}

	public List<Book> getBooks() {

	}

	boolean connected() {

	}

}
