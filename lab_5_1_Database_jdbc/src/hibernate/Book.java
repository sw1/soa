package hibernate;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Book {
	private String author = null;
	private String title = null;
	private Integer year = null;
	@Id
	private String isbn = null;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
}
