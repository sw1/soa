package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;

@ApplicationScoped
public class DBManager {
	String url = null;
	String login = null;
	String pass = null;

	Connection conn = null;

	public DBManager() {
		createConnection();
	}
	
	public void connect(String url, String login, String pass) {
		this.url = url;
		this.login = login;
		this.pass = pass;
		createConnection();
	}
		

	public void createConnection() {
		try {
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection(url, login, pass);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Book getBook(String isbn){
		Book b= new Book();
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeQuery("SELECT isbn, author, title, year FROM books WHERE isbn="+isbn+";");
			ResultSet rs = stmt.getResultSet();
			while (rs.next()) {
				b.setIsbn(rs.getString(1));
				b.setAuthor(rs.getString(2));
				b.setTitle(rs.getString(3));
				b.setYear(rs.getInt(4));
			}
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}
	
	public void editBook(Book b){
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate("UPDATE books SET author=\'"+b.getAuthor()+"\', title=\'"+b.getTitle()+"\', year="+b.getYear()+";");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void addBook(Book b){
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate("INSERT INTO books(isbn,author,title,year) VALUES (\'"+b.getIsbn()+"\','"+b.getAuthor()+"\', \'"+b.getTitle()+"\',"+b.getYear()+");");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void removeBook(String isbn){
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate("DELETE from books WHERE isbn='"+isbn+"\';");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Book> getBooks() {
		List<Book> list = new ArrayList<Book>();
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeQuery("SELECT isbn, author, title, year FROM books;");
			ResultSet rs = stmt.getResultSet();
			while (rs.next()) {
				Book b = new Book();
				b.setIsbn(rs.getString(1));
				b.setAuthor(rs.getString(2));
				b.setTitle(rs.getString(3));
				b.setYear(rs.getInt(4));
				list.add(b);
			}
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

}
