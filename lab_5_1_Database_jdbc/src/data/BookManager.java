package data;

import hibernate.Book;
import hibernate.DBManager;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class BookManager
 */
@WebServlet("/BookManager")
public class BookManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	DBManager db;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookManager() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String isbn=request.getParameter("isbn");
		if(action.equals("edit") || action.equals("add")){
			Book b=new Book();
			b.setIsbn(isbn);
			b.setTitle(request.getParameter("title"));
			b.setAuthor(request.getParameter("author"));
			b.setYear(Integer.parseInt(request.getParameter("year")));
			RequestDispatcher view = request.getRequestDispatcher("bookEdit.jsp");
			view.forward(request, response);
		}else if (action.equals("remove")){
			
			RequestDispatcher view = request.getRequestDispatcher("BookManager");
			view.forward(request, response);
		} else {
			
		}
	}

}
