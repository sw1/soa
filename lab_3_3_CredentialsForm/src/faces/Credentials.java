package faces;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

@ManagedBean
@SessionScoped
public class Credentials {
	private String name = null;
	private String lastName = null;
	private Integer age = null;
	private String sex = null;
	private String voivodship = null;
	private Boolean confirmButtonDisabled=true;
	private static List<String> sexes = null;
	private static List<String> voivodships = null;
	private static final long minAge=0L;
	private static final long maxAge=120L;
	
	public Credentials(){
		if(sexes==null){
			sexes=new ArrayList<String>();
			sexes.add("male");
			sexes.add("female");
		}
		if(voivodships==null){
			voivodships=new ArrayList<String>();
			voivodships.add("");
			voivodships.add("malopolskie");
			voivodships.add("slaskie");
			voivodships.add("podkarpackie");
		}
	}
	
	public String confirm(){
		
		return "print.xhtml";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getVoivodship() {
		return voivodship;
	}

	public void setVoivodship(String voivodship) {
		if(voivodship.equals("")){
			setConfirmButtonDisabled(true);
		}else{
			setConfirmButtonDisabled(false);
		}
		this.voivodship = voivodship;
	}

	public List<String> getSexes() {
		return sexes;
	}

	public List<String> getVoivodships() {
		return voivodships;
	}


	public long getMinAge() {
		return minAge;
	}

	public long getMaxAge() {
		return maxAge;
	}

	public Boolean getConfirmButtonDisabled() {
		return confirmButtonDisabled;
	}

	public void setConfirmButtonDisabled(Boolean confirmButtonDisabled) {
		this.confirmButtonDisabled = confirmButtonDisabled;
	}
	
	
	
	
}
