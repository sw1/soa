<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<form action="GuestBook" method="post">
Name:<input type="text" name="name" /><br />
Email:<input type="text" name="email" /><br />
Comment:<input type="text" name="comment" /><br />
<input type="submit" />
</form>
<c:forEach var="guest" items="${guests}">
<p><span style="font-weight: bold;" >${guest.name}</span> (${guest.email}) says <br /> ${guest.comment}</p>
</c:forEach>
</body>
</html>