package data;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GuestBook
 */
@WebServlet("/GuestBook")
public class GuestBook extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static Vector<GuestBook.Entry> guests=new Vector<GuestBook.Entry>();
    
    
    public class Entry{
    	private String name=null;
    	private String email=null;
    	private String comment=null;
    	
    	public Entry(String name, String email, String comment){
    		this.name=name;
    		this.email=email;
    		this.comment=comment;
    	}
    	
		public String getName() {
			return name;
		}
		public String getEmail() {
			return email;
		}
		public String getComment() {
			return comment;
		}
    	
    }
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GuestBook() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("guests", guests);
		RequestDispatcher view = request.getRequestDispatcher("index.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("name");
		String email=request.getParameter("email");
		String comment=request.getParameter("comment");
		guests.add(new Entry(name, email, comment));
		request.setAttribute("guests", guests);
		RequestDispatcher view = request.getRequestDispatcher("index.jsp");
		view.forward(request, response);
	}

}
