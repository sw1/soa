package expenses;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EntryManager
 */
@WebServlet("/EntryManager")
public class EntryManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EntryManager() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String amount=request.getParameter("amount");
		ReadWrite rw= new ReadWrite();
		if(amount.equals("single")){
			String name=request.getParameter("name");
			Double value=Double.parseDouble(request.getParameter("value"));
			rw.addEntry(name, value);
			rw.save();
		}
		if(amount.equals("bulk")){
			Lock l=new Lock();
			l.setLocked(false);
			String names[]=request.getParameterValues("name[]");
			String values[]=request.getParameterValues("value[]");
			rw.clear();
			for(int i=0;i<names.length;i++){
				String name=names[i];
				Double value=Double.parseDouble(values[i]);
				rw.addEntry(name, value);
			}
			rw.save();
		}
		response.sendRedirect("index.jsp");
	}

}
