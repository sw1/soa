package expenses;

public class Lock {
	static boolean locked=false;

	public boolean getLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		Lock.locked = locked;
	}
	
}
