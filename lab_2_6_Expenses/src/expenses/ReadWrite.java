package expenses;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Vector;

public class ReadWrite {
	private static File file;
	private static String text;
	private static Vector<Entry> vec;
	
	public ReadWrite(){
		file=new File("expenses.csv");
		text=new String();
		vec=new Vector<Entry>();
		if(! file.exists()){
			try {
				if(!file.createNewFile()){
					System.err.println("Cannot create file!");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(file.getAbsolutePath());
		readFile();
	}
	
	public void readFile(){
		try (BufferedReader reader = Files.newBufferedReader(file.toPath(), Charset.defaultCharset())) {
		    String line = null;
		    while ((line = reader.readLine()) != null) {
				String s[]= line.split(":");
				addEntry(s[0], Double.parseDouble(s[1]));
		    }
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);
		}
	}
	
	public void addEntry(String name, Double value){
		vec.add(new Entry(name, value));
		text+="\n"+name+":"+value;
	}
	
	public String getText(){
		return text;
	}
	
	public Vector<Entry> getExpenses(){
		return vec;
	}
	
	public void clear(){
		vec.clear();
	}
	
	public void save(){
		try (BufferedWriter writer = Files.newBufferedWriter(file.toPath(), Charset.defaultCharset())) {
			for(Entry e:vec){
				String s=e.getName()+":"+e.getValue()+"\n";
				writer.write(s, 0, s.length());
			}
			writer.close();
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);
		}
	}
	
}
