<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isThreadSafe="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="reader" class="expenses.ReadWrite"></jsp:useBean>
<jsp:useBean id="lock" class="expenses.Lock"></jsp:useBean>
<c:choose>
<c:when test="${lock.locked==false }">
<jsp:setProperty name="lock" property="locked" value="true"/>
<form action="EntryManager" method="post">
<table>
<tr><td>Name</td><td>Value</td></tr>
<c:forEach var="item" items="${reader.expenses }">
<tr><td><input type="text" name="name[]" value="${item.name}"/></td><td><input type="text" name="value[]" value="${item.value}"/></td></tr>
</c:forEach>
</table>
<input type="hidden" name="amount" value="bulk" />
<input type="submit" />
</form>
</c:when>
<c:otherwise>Strona edytowana przez innego użytkownika</c:otherwise>
</c:choose>
</body>
</html>