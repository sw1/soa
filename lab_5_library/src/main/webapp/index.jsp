<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:useBean id="db" scope="application" class="library.DBManager"></jsp:useBean>
	<table>
		<c:forEach items="${db.books}" var="book">
			<tr>
				<td>${book.isbn}</td>
				<td>${book.title}</td>
				<td>${book.author}</td>
				<td>${book.year}</td>
				<td><form action="BookActionManagerServlet" method="post">
						<input type="hidden" name="isbn" value="${book.isbn}" /> <input
							type="hidden" name="action" value="edit" /> <input type="submit"
							value="Edit" />
					</form>
				<td><form action="BookActionManagerServlet" method="post">
						<input type="hidden" name="isbn" value="${book.isbn}" /> <input
							type="hidden" name="action" value="remove" /> <input
							type="submit" value="Remove" />
					</form></td>
			</tr>
		</c:forEach>
	</table>
	<form action="BookActionManagerServlet" method="post">
		<input type="hidden" name="action" value="add" /> <input
			type="submit" value="Add">
	</form>
</body>
</html>