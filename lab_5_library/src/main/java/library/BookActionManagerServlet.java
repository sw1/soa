package library;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class BookActionManagerServlet
 */
@WebServlet("/BookActionManagerServlet")
public class BookActionManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	DBManager db;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookActionManagerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		Book b = null;
		if (action.equals("edit")) {
			String isbn = request.getParameter("isbn");
			b = db.getBook(isbn);
		} else {
			b = new Book();
		}
		request.setAttribute("book", b);
		if (action.equals("edit") || action.equals("add")) {
			RequestDispatcher view = request
					.getRequestDispatcher("bookEdit.jsp");
			view.forward(request, response);
		} else if (action.equals("remove")) {
			RequestDispatcher view = request
					.getRequestDispatcher("BookManager");
			view.forward(request, response);
		}

	}
}
