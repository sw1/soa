package library;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class DBManager {

	private static SessionFactory factory = HibernateUtil.getSessionFactory();

	public List<Book> getBooks() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Book> list = null;
		tx = session.beginTransaction();
		Criteria cr = session.createCriteria(Book.class);
		list = cr.list();
		tx.commit();
		session.close();
		return list;
	}

	public Book getBook(String isbn) {
		Session session = factory.openSession();
		Transaction tx = null;
		Book b = null;
		tx = session.beginTransaction();
		b = (Book) session.get(Book.class, isbn);
		tx.commit();
		session.close();
		return b;
	}

	public void addOrEditBook(Book b) {
		Session session = factory.openSession();
		Transaction tx = null;
		tx = session.beginTransaction();
		session.saveOrUpdate(b);
		tx.commit();
		session.close();
	}

	public void remove(String isbn) {
		Session session = factory.openSession();
		Transaction tx = null;
		tx = session.beginTransaction();
		String hql = "DELETE FROM books " + "WHERE isbn = :isbn";
		Query query = session.createQuery(hql);
		query.setParameter("isbn", isbn);
		int result = query.executeUpdate();
		System.out.println("Rows affected: " + result);
		tx.commit();
		session.close();
	}
}
