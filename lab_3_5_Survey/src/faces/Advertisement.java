package faces;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class Advertisement {
	static int count=0;
	private Random rand=new Random();
	private static List<String> ads=null;
	
	public Advertisement(){
		if(ads==null){
			ads=new ArrayList<String>();
			ads.add("washing powder");
			ads.add("pills");
			ads.add("car");
			ads.add("cloth");
		}
	}
	
	public String getAd(){
		return ads.get(rand.nextInt(ads.size()));
	}
	
	public void addonClicked(){
		count++;
		System.out.println("Ad clock count:"+count);
	}
	
}
