package faces;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Survey {
	private String clientType=null;
	private String name=null;
	private String email=null;
	private String postcode=null;
	private String age=null;
	private String sex=null;
	private String education=null;
	private String size=null;
	private String clothesPrice=null;
	private String buyPlace=null;
	private String buyFrequency=null;
	private String clothesColor=null;
	private String clothesType = null;
	private String lastBuy=null;
	private String buySatisfaction=null;
	private String employeeSatisfaction=null;
	private String remarks=null;
	private static List<String> maleClothes=null;
	private static List<String> femaleClothes=null;
	
	public Survey(){
		if(maleClothes==null){
			maleClothes=new ArrayList<String>();
			maleClothes.add("Suits");
			maleClothes.add("Trousers");
			maleClothes.add("Shorts");
			maleClothes.add("Shirts");
			maleClothes.add("Ties");
		}
		if(femaleClothes==null){
			femaleClothes=new ArrayList<String>();
			femaleClothes.add("Suits");
			femaleClothes.add("Trousers");
			femaleClothes.add("Blouses");
			femaleClothes.add("Skirts");
		}
	}
	
	public List<String> getClothesTypes(){
		if(sex.equals("male")){
			return maleClothes;
		}else if (sex.equals("female")){
			return femaleClothes;
		}
		return null;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getClothesPrice() {
		return clothesPrice;
	}
	public void setClothesPrice(String clothesPrice) {
		this.clothesPrice = clothesPrice;
	}
	public String getBuyPlace() {
		return buyPlace;
	}
	public void setBuyPlace(String buyPlace) {
		this.buyPlace = buyPlace;
	}
	public String getBuyFrequency() {
		return buyFrequency;
	}
	public void setBuyFrequency(String buyFrequency) {
		this.buyFrequency = buyFrequency;
	}
	public String getClothesColor() {
		return clothesColor;
	}
	public void setClothesColor(String clothesColor) {
		this.clothesColor = clothesColor;
	}
	public String getClothesType() {
		return clothesType;
	}
	public void setClothesType(String clothesType) {
		this.clothesType = clothesType;
	}
	public String getLastBuy() {
		return lastBuy;
	}
	public void setLastBuy(String lastBuy) {
		this.lastBuy = lastBuy;
	}
	public String getBuySatisfaction() {
		return buySatisfaction;
	}
	public void setBuySatisfaction(String buySatisfaction) {
		this.buySatisfaction = buySatisfaction;
	}
	public String getEmployeeSatisfaction() {
		return employeeSatisfaction;
	}
	public void setEmployeeSatisfaction(String employeeSatisfaction) {
		this.employeeSatisfaction = employeeSatisfaction;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getClientType() {
		return clientType;
	}
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	
	
	
}
