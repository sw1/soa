package faces;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Survey {

	private String name = null;
	private String lastName = null;
	private String address = null;
	private String phone = null;
	private String email = null;
	private Integer age = null;
	private Integer height = null;
	private Integer weight = null;
	private String eyesColor = null;
	private String hairColor = null;
	private Integer clothSize = null;
	private Integer shoeSize = null;
	private Integer Experience = null;
	private String sex = null;
	private Integer bustSize = null;
	private Integer braSize = null;
	private Integer waist = null;
	private Integer hip = null;
	private Integer legLength = null;
	private Integer chest = null;
	private static List<String> sexes = null;
	private static List<Integer> experiences = null;
	private static int minHeight = 50;
	private static int maxHeight = 250;
	private static int minWeight = 40;
	private static int maxWeight = 150;

	public Survey() {
		if (sexes == null) {
			sexes = new ArrayList<String>();
			sexes.add("");
			sexes.add("male");
			sexes.add("female");
		}
		if (experiences == null) {
			experiences = new ArrayList<Integer>();
			experiences.add(1);
			experiences.add(2);
			experiences.add(3);
			experiences.add(4);
			experiences.add(5);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public String getEyesColor() {
		return eyesColor;
	}

	public void setEyesColor(String eyesColor) {
		this.eyesColor = eyesColor;
	}

	public String getHairColor() {
		return hairColor;
	}

	public void setHairColor(String hairColor) {
		this.hairColor = hairColor;
	}

	public Integer getClothSize() {
		return clothSize;
	}

	public void setClothSize(Integer clothSize) {
		this.clothSize = clothSize;
	}

	public Integer getShoeSize() {
		return shoeSize;
	}

	public void setShoeSize(Integer shoeSize) {
		this.shoeSize = shoeSize;
	}

	public Integer getExperience() {
		return Experience;
	}

	public void setExperience(Integer experience) {
		Experience = experience;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getBustSize() {
		return bustSize;
	}

	public void setBustSize(Integer bustSize) {
		this.bustSize = bustSize;
	}

	public Integer getBraSize() {
		return braSize;
	}

	public void setBraSize(Integer braSize) {
		this.braSize = braSize;
	}

	public Integer getWaist() {
		return waist;
	}

	public void setWaist(Integer waist) {
		this.waist = waist;
	}

	public Integer getHip() {
		return hip;
	}

	public void setHip(Integer hip) {
		this.hip = hip;
	}

	public Integer getLegLength() {
		return legLength;
	}

	public void setLegLength(Integer legLength) {
		this.legLength = legLength;
	}

	public Integer getChest() {
		return chest;
	}

	public void setChest(Integer chest) {
		this.chest = chest;
	}

	public List<String> getSexes() {
		return sexes;
	}

	public List<Integer> getExperiences() {
		return experiences;
	}

	public int getMinHeight() {
		return minHeight;
	}

	public int getMaxHeight() {
		return maxHeight;
	}

	public int getMinWeight() {
		return minWeight;
	}

	public int getMaxWeight() {
		return maxWeight;
	}

}
