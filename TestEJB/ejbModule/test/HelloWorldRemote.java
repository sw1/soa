package test;

import javax.ejb.Remote;

@Remote
public interface HelloWorldRemote {
	public String printHelloWorld();
}
