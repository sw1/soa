package car;

import java.util.ArrayList;
import java.util.List;

public class CarChoiceHelper {
	
	public List<String> getCar(String carType, Integer priceRange){
		List<String> cars=new ArrayList<String>();
		if(carType.equals("sportowy") && priceRange<10000){
			cars.add("mazda");
			cars.add("honda");
		}else if(carType.equals("sportowy")){
			cars.add("bmw");
			cars.add("porsche");
		}else if(carType.equals("miejski") && priceRange<10000){
			cars.add("fiat");
			cars.add("renault");
		}else if(carType.equals("miejski")){
			cars.add("kia");
			cars.add("toyota");
		}else if(carType.equals("luksusowy") && priceRange<10000){
			cars.add("daewoo");
			cars.add("citroen");
		}else if(carType.equals("luksusowy")){
			cars.add("mercedes");
			cars.add("rols royce");
		}
		return cars;
	}
}
