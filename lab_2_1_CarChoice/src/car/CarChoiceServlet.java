package car;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CarChoiceServlet
 */
@WebServlet("/CarChoiceServlet")
public class CarChoiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CarChoiceServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String carType=request.getParameter("carType");
		String priceRange=request.getParameter("priceRange");
		PrintWriter out=response.getWriter();
		out.println("Car type: "+carType+"<br />");
		out.println("Price range: "+priceRange+"<br />");
		CarChoiceHelper cch=new CarChoiceHelper();
		out.println("Recomended brand:"+cch.getCar(carType, Integer.parseInt(priceRange)));
	}

}
