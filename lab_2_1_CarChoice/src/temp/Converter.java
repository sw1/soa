package temp;

import javax.ejb.LocalBean;
import javax.ejb.RemoteHome;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class Converter
 */
@Stateless
@LocalBean
@RemoteHome(ConverterRemoteHome.class)
public class Converter implements ConverterRemote {

    /**
     * Default constructor. 
     */
    public Converter() {
        // TODO Auto-generated constructor stub
    }

}
