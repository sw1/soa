package temp;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface ConverterRemoteHome extends EJBHome {

	public ConverterRemoteComponent create() throws CreateException, RemoteException;
	
}
