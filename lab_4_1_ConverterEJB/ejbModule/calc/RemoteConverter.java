package calc;

import javax.ejb.Remote;

@Remote

public interface RemoteConverter {

	public double Fahr2Cels(double temp);
	public double Cels2Fahr(double temp);
}
