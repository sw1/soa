package calc;

import calc.RemoteConverter;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class Converter
 */
@Stateless(name="degreeConverter")
@Remote(RemoteConverter.class)
public class DegreeConverter implements RemoteConverter {

	private final static double fConst=5/9.;
	private final static double cConst=9/5.;
	
    /**
     * Default constructor. 
     */
    public DegreeConverter() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public double Fahr2Cels(double temp) {
		return fConst*(temp-32);
	}

	@Override
	public double Cels2Fahr(double temp) {
		return cConst*temp+32;
	}

}
